package com.woniu.req;


import lombok.Data;

@Data
public class Header {

    private String token;
}
