package com.woniu.thread;

import com.alibaba.fastjson.JSONObject;
import com.woniu.dao.UserDao;
import com.woniu.vo.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;



@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class FastJonTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void testShardingJDBCInsert() {
        User user = new User();
        for(int i=0; i<10; i++) {
            user.setName("小明" + i);
            user.setSex(1);
            if(userDao.insertUser(user) == 1) {
                log.info("插入成功！");
            } else {
                log.info("插入失败！");
            }
        }

    }



    @Test
    public void testShardingJDBCSelect() {
        User user = new User();
        user.setSex(1);
        user.setId(888188847249162241L);
        List<User> userList = userDao.selectUser(user);
        log.info("查询结果：{}", JSONObject.toJSONString(userList));
    }




}
