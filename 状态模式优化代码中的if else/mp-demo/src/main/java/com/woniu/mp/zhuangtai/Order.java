package com.woniu.mp.zhuangtai;

import lombok.Data;


@Data
public class Order {

    private OrderStateEnum state;

    public Order() {
        state = OrderStateEnum.UNPAY;
    }

    public void nextState(){
        state.nextState(this);
    }
}