package com.woniu.controller;

import com.woniu.service.MainService;
import com.woniu.vo.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * IP 归属地获取，一个依赖轻松搞定
 */
@RequestMapping("/api")
@RestController
@Slf4j
public class AddressController {


    @Autowired
    MainService mainService;


    @GetMapping("/get2")
    public ApiResponse get() {
        mainService.test1();
        return ApiResponse.ok(null);
    }




}
