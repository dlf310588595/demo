package com.woniu.enumd;

public enum AnimalEnum implements Eat {
    Dog(){
        @Override
        public void eat() {
            System.out.println("吃骨头");
        }
    },

    Cat() {
        @Override
        public void eat() {
            System.out.println("吃鱼干");
        }
    },

    Sheep() {
        @Override
        public void eat() {
            System.out.println("吃草");
        }
    }
}