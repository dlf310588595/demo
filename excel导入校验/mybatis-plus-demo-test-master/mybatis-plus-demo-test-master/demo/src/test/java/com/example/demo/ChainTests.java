package com.example.demo;

import com.example.demo.chain.ProductService;
import com.example.demo.chain.ProductVO;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Test;
import javax.annotation.Resource;
import java.math.BigDecimal;

@SpringBootTest
public class ChainTests {

    @Resource
    ProductService productService;

    @Test
    void contextLoads() {
        ProductVO param = ProductVO.builder()
                .skuId(11L)
                .skuName("测试商品")
                .imgPath("http://..")
                .price(new BigDecimal(13))
                .stock(-13)
                .build();
        productService.createProduct(param);
    }
}
