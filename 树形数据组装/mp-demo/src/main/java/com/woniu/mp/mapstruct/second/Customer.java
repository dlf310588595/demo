package com.woniu.mp.mapstruct.second;

import lombok.Data;

@Data
public class Customer {
 private String id;
 private String name;
 private TimeAndFormat timeAndFormat;
}
