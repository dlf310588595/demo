package com.woniu.mp.mapstruct.four;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public abstract class StudentMapper {

    @Autowired
    protected SimpleService simpleService;

    /**
     * 转换的时候，如何注入springBean
     */
    @Mapping(target = "name", expression = "java(simpleService.formatName(source.getName()))")
    public abstract Student map(StudentDto source);
}
