package com.woniu.mp.mapstruct.second;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.UUID;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, imports = UUID.class)
public interface CustomerMapper {

    /**
     * 场景二：转换中调用表达式
     */
    @Mapping(target = "timeAndFormat",
            expression = "java( new TimeAndFormat( s.getTime(), s.getFormat() ) )")
    @Mapping(target = "id", source = "id", defaultExpression = "java( UUID.randomUUID().toString() )")
    Customer toCustomer(CustomerDto s);

}

