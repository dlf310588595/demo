package com.woniu.strategy;


public interface TransferService {
    String transfer();

    CodeEnum transCode();
}
